from django import forms
from .models import RadiusUser


class RadiusUserRegisterForm(forms.ModelForm):
    class Meta:
        model = RadiusUser
        fields = (
            'username',
            'password',
            'email',
            'nama_lengkap',
            'nik_or_nim',
            'no_hp',
            'jenis_kelamin',
            'jurusan',
            'semester',
            'alamat',
        )