from django.contrib import admin
from .models import RadiusUser

@admin.register(RadiusUser)
class RadiusUserAdmin(admin.ModelAdmin):
    model = RadiusUser
    
    list_display = (
        'username',
        'email',
        'jenis_user',
        'no_hp',
        'jenis_kelamin',
        'jurusan',
        'semester',
        'active_user',
        'created_at',
        'updated_at'
    )
    
    list_filter = ('jenis_kelamin', 'jurusan', 'active_user')
    
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('Personal Info', {'fields': ('email', 'jenis_kelamin', 'no_hp', 'alamat', 'jurusan', 'semester')}),
        ('Status Permission', {'fields': ('jenis_user', 'active_user')}),
    )
    readonly_fields = ('password',)