import hashlib
from typing import Optional
from django.http import HttpRequest
from .models import RadiusUser


def cek_password(password: str, encoded):
    create_pass = hashlib.sha256(password.encode()).hexdigest()
    if create_pass == encoded:
        return True
    else:
        return False


def create_password(raw_password: str):
    return hashlib.sha256(raw_password.encode()).hexdigest()


class RadiusUserBackend(object):
    
    MODEL = RadiusUser
    
    def authenticate(
        self,
        request: HttpRequest,
        username: Optional[str] = None,
        password: Optional[str] = None,
        ):
        if username and password:
            try:
                user = self.MODEL.objects.get(username=username)
                if cek_password(password=password, encoded=user.password):
                    return user
                return None
            except self.MODEL.DoesNotExist:
                return None
        return None
    
    def get_user(self, user_id):
        try:
            user = self.MODEL.objects.get(pk=user_id)
            return user
        except self.MODEL.DoesNotExist:
            return None
        except self.MODEL.MultipleObjectsReturned:
            return None