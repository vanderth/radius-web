from django.http import HttpRequest, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.conf import settings
from .models import RadiusUser
from .forms import RadiusUserRegisterForm
from .backends import create_password


def radius_login(request):
    if not request.user.is_authenticated:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username=username, password=password)
            if user:
                if user.is_active:
                    login(request, user)
                    messages.success(request, 'Login Berhasil!')
                    return redirect(settings.LOGIN_REDIRECT_URL)
                else:
                    context = {'message': 'Username atau password salah!'}
                    return render(request, 'accounts/login.html', context)
                
        if request.method == 'GET':
            return render(request, 'accounts/login.html', {})
    return redirect(settings.LOGIN_REDIRECT_URL)
    
    
def radius_register(request):
    if request.method == 'POST':
        form = RadiusUserRegisterForm(request.POST)
        if form.is_valid():
            form.save(commit=False)
            password = form.cleaned_data['password']
            form.password = create_password(password) # type: ignore
            form.save(commit=True)
            messages.success(request, 'Registrasi Berhasil, silahkan login dengan username & password anda.')
            return HttpResponseRedirect(settings.LOGIN_URL)
        
        context = {
            'message': 'Registrasi gagal, mohon periksa kembali data anda.',
            'form': RadiusUserRegisterForm()
            }
        return render(request, 'accounts/registrasi.html', context)
    
    return render(request, 'accounts/registrasi.html', {'form': RadiusUserRegisterForm()})


def radius_logout(request):
    if request.user.is_authenticated:
        logout(request)
        messages.success(request, "Done, Anda telah logout!")
        return redirect('accounts:login')
    messages.error(request, 'Anda belum login.')
    return redirect(settings.LOGIN_URL)