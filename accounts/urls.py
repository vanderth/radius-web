from django.urls import path
from . import views

app_name = 'accounts'

urlpatterns = [
    path('login/', views.radius_login, name='radius_login'),
    path('register/', views.radius_register, name='radius_register'),
    path('logout/', views.radius_logout, name='radius_logout'),
]
