from django.db import models

    
class RadiusUser(models.Model):
    JENIS_KELAMIN = [
        ('', '--Pilih Jenis Kelamin--'),
        ('Pria', 'Pria'),
        ('Wanita', 'Wanita'),
    ]
    
    JENIS_USER = [
        ('MAHASISWA', 'MAHASISWA'),
        ('POLTEKBA', 'POLTEKBA'),
    ]
    
    username = models.CharField(max_length=32)
    password = models.CharField(max_length=128)
    email = models.CharField(max_length=64, null=True, blank=True)
    nama_lengkap = models.CharField(max_length=128, null=True, blank=True)
    nik_or_nim = models.CharField(max_length=64, null=True, blank=True)
    jenis_user = models.CharField(
        max_length=32,
        choices=JENIS_USER,
        default='MAHASISWA',
        null=True,
        blank=True
    )
    no_hp = models.CharField(
        max_length=32,
        null=True,
        blank=True
        )
    jenis_kelamin = models.CharField(
        max_length=32,
        choices=JENIS_KELAMIN,
        null=True,
        blank=True
        )
    jurusan = models.CharField(
        max_length=100,
        null=True,
        blank=True
        )
    semester = models.CharField(
        max_length=48,
        null=True,
        blank=True,
        )
    active_user = models.BooleanField(default=True)
    alamat = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.username
    
    def set_password(self, raw_password):
        from accounts.backends import create_password
        self.radius_password = create_password(raw_password)
        
    def check_password(self, raw_password):
        from accounts.backends import cek_password
        return cek_password(raw_password, self.radius_password)
    
    @property
    def is_active(self):
        return self.active_user == True
    
    @property
    def is_authenticated(self) -> bool:
        return True
    
    @property
    def is_anonymous(self) -> bool:
        return False
    