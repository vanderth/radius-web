from django.urls import path
from .views import AccountingView, PostAuthView

app_name = 'radius'

urlpatterns = [
    path('usages', AccountingView.as_view(), name='usages-view'),
    path('log-auth', PostAuthView.as_view(), name='log-auth-view')
]
