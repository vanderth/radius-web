import hashlib
from django.db.models.signals import pre_save, post_save, post_delete
from django.dispatch import receiver

from accounts.models import User
from .models import Radcheck

@receiver(post_save, sender=User)
def create_user_radius(sender, instance, created, **kwargs):
    if created:
        Radcheck.objects.create(
            username=instance.username,
            attribute = "SHA2-Password",
            op=":=",
            value=instance.password            
            )
        
        
@receiver(pre_save, sender=User)
def check_user(sender, instance, **kwargs):
    """
    Create user with standard sha256 password
    """
    if instance.id is None:
        create_user_radius = Radcheck.objects.create(
            username = instance.username,
            attribute = "SHA2-Password",
            op = ":=",
            value = instance.password,
        )
        create_user_radius.save()