from django.contrib import admin
from .models import (
    Nas,
    Radacct,
    Radcheck,
    Radgroupcheck,
    Radgroupreply,
    Radpostauth,
    Radreply,
    Radusergroup
)

admin.site.register(Nas)
admin.site.register(Radacct)
admin.site.register(Radcheck)
admin.site.register(Radgroupcheck)
admin.site.register(Radgroupreply)


@admin.register(Radpostauth)
class RadiusPostAdmin(admin.ModelAdmin):
    list_display = ('username', 'reply', 'authdate')


admin.site.register(Radreply)
admin.site.register(Radusergroup)