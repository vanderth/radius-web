from typing import Union
from datetime import datetime
from django.views.generic import ListView
from django.conf import settings
from django.http import HttpRequest
from django.db.models import Sum
from django.utils.translation import ngettext, gettext_lazy
from django.shortcuts import render, redirect
from .models import Radcheck, Radacct, Radpostauth
        

def trafficsizeformat(bytes: Union[float, int]):
    try:
        bytes = float(bytes)
    except (TypeError, ValueError, UnicodeDecodeError):
        return u"0 bytes"
    
    if bytes < 1024:
        return ngettext("%(size)d byte", "%(size)d bytes", bytes) % {'size': bytes}
    if bytes < 1024 * 1024:
        return gettext_lazy("%.1f KB") % (bytes / 1024)
    if bytes < 1024 * 1024 * 1024:
        return gettext_lazy("%.1f MB") % (bytes / (1024 * 1024))
    return gettext_lazy("%.1f GB") % (bytes / (1024 * 1024 * 1024))


class AccountingView(ListView):
    model = Radacct
    template_name = 'radius/accounting_view.html'
    paginate_by = settings.PAGINATION_PER_PAGE
    
    def get_queryset(self):
        if self.request.user.is_staff or self.request.user.is_superuser: # type: ignore
            return Radacct.objects.all().order_by('-pk')
        else:
            return Radacct.objects.filter(username=self.request.user.username).order_by('-pk') # type: ignore
        
    def get_context_data(self, **kwargs):
        context = super(AccountingView, self).get_context_data(**kwargs)
        total_record = Radacct.objects.all().count()
        upload = Radacct.objects.all().aggregate(Sum('acctinputoctets')).get('acctinputoctets__sum')
        download = Radacct.objects.all().aggregate(Sum('acctoutputoctets')).get('acctoutputoctets__sum')
        context.update({
            'total_record': total_record,
            'total_upload': trafficsizeformat(upload), # type: ignore
            'total_download': trafficsizeformat(download), # type: ignore
        })
        return context
        
class PostAuthView(ListView):
    model = Radpostauth
    template_name = 'radius/postauth_view.html'
    # paginate_by = settings.PAGINATION_PER_PAGE
    paginate_by = 5
    
    def get_queryset(self):
        return Radpostauth.objects.all().order_by('-authdate')
    
    def get_context_data(self, **kwargs):
        context = super(PostAuthView, self).get_context_data(**kwargs)
        context.update({
            'total_record': Radpostauth.objects.all().count(),
            'total_accept': Radpostauth.objects.filter(reply='Access-Accept').count(),
            'total_reject': Radpostauth.objects.filter(reply='Access-Reject').count(),
        })
        return context
    
    
def user_search(request: HttpRequest):
    current_time = datetime.now()
    if request.method == "POST":
        username = request.POST.get('username')
        bulan = request.POST.get('month')
        
        if username is not None:
            total_upload = Radacct.objects.filter(username=username, acctstarttime__month=current_time.month)
            return total_upload