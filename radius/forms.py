from django import forms
from .models import Radcheck, Radacct


class RadiusUser(forms.ModelForm):
    op = forms.CharField(widget=forms.Select(choices=Radcheck.OP_OPTIONS))
    class Meta:
        model = Radcheck
        fields = ['username', 'attribute', 'op', 'value']
        
        
        
class RadiusUserLogin(forms.ModelForm):
    
    class Meta:
        model = Radcheck
        fields = ['username', 'value']